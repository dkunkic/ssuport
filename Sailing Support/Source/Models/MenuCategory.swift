//
//  MenuCategory.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation

class MenuCategory: NSObject, NSCoding {
    
    var categoryId: Int?
    var parentId: Int?
    var categoryName: String?
    var categoryDesc: String?
    var customPage: String?
    var imgSrc: String?
    var wikiCategoryId: Int?
    var templateId: Int?
    var locationId: Int?
    var iconId: Int?
    var display: Int?
    var newWindow: Int?
    var orderNum: Int?
    var wikiId: Int?
    var name: String?
    var icon: String?
    
    var children: [MenuCategory]?
    
    override init() {
        categoryId = 0
        parentId = 0
        categoryName = ""
        categoryDesc = ""
        customPage = ""
        imgSrc = ""
        wikiCategoryId = 0
        templateId = 0
        locationId = 0
        iconId = 0
        display = 0
        newWindow = 0
        orderNum = 0
        wikiId = 0
        name = ""
        icon = ""
    }
    
    init(categoryName: String, categoryId: Int, icon: String) {
        super.init()
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.icon = icon
    }
    
    init(categoryName: String, categoryId: Int, icon: String, children: [MenuCategory]) {
        super.init()
        self.categoryName = categoryName
        self.categoryId = categoryId
        self.icon = icon
        self.children = children
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.categoryId, forKey: "categoryId")
        aCoder.encode(self.parentId, forKey: "parentId")
        aCoder.encode(self.categoryName, forKey: "categoryName")
        aCoder.encode(self.categoryDesc, forKey: "categoryDesc")
        aCoder.encode(self.customPage, forKey: "customPage")
        aCoder.encode(self.imgSrc, forKey: "imgSrc")
        aCoder.encode(self.wikiCategoryId, forKey: "wikiCategoryId")
        aCoder.encode(self.locationId, forKey: "locationId")
        aCoder.encode(self.iconId, forKey: "iconId")
        aCoder.encode(self.display, forKey: "display")
        aCoder.encode(self.newWindow, forKey: "newWindow")
        aCoder.encode(self.orderNum, forKey: "orderNum")
        aCoder.encode(self.wikiId, forKey: "wikiId")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.icon, forKey: "icon")
        aCoder.encode(self.children, forKey: "children")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.categoryId = (aDecoder.decodeObject(forKey: "categoryId") as? Int) ?? 0
        self.parentId = (aDecoder.decodeObject(forKey: "parentId") as? Int) ?? 0
        self.categoryName = (aDecoder.decodeObject(forKey: "categoryName") as? String) ?? ""
        self.categoryDesc = (aDecoder.decodeObject(forKey: "categoryDesc") as? String) ?? ""
        self.customPage = (aDecoder.decodeObject(forKey: "customPage") as? String) ?? ""
        self.imgSrc = (aDecoder.decodeObject(forKey: "imgSrc") as? String) ?? ""
        self.wikiCategoryId = (aDecoder.decodeObject(forKey: "wikiCategoryId") as? Int) ?? 0
        self.templateId = (aDecoder.decodeObject(forKey: "templateId") as? Int) ?? 0
        self.locationId = (aDecoder.decodeObject(forKey: "locationId") as? Int) ?? 0
        self.iconId = (aDecoder.decodeObject(forKey: "iconId") as? Int) ?? 0
        self.display = (aDecoder.decodeObject(forKey: "display") as? Int) ?? 0
        self.newWindow = (aDecoder.decodeObject(forKey: "newWindow") as? Int) ?? 0
        self.orderNum = (aDecoder.decodeObject(forKey: "orderNum") as? Int) ?? 0
        self.wikiId = (aDecoder.decodeObject(forKey: "wikiId") as? Int) ?? 0
        self.name = (aDecoder.decodeObject(forKey: "name") as? String) ?? ""
        self.icon = (aDecoder.decodeObject(forKey: "icon") as? String) ?? ""
        self.children = (aDecoder.decodeObject(forKey: "children") as? [MenuCategory]) ?? []
    }

}

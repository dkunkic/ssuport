//
//  LocationsRootModel.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class LocationRootModel: Mappable {
    
    var error: Bool?
    var root: [LocationModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        root <- map["root"]
    }
    
}

//
//  WeatherHourItem.swift
//  Sailing Support
//
//  Created by Danijel on 14/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherHourItem: NSObject {
    
    var epochDateTime: Int?
    var weatherIcon: Int?
    var iconPhrase: String?
    var temperature: WeatherTemperature?
    var realFeelTemperature: WeatherTemperature?
    var wind: WeatherWind?
    var relativeHumidity: Int?
    var visibility: WeatherVisibility?
    var uvIndex: Int?
    
}

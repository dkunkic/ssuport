//
//  WeatherDay.swift
//  Sailing Support
//
//  Created by Danijel on 16/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherDay: NSObject {
    
    var icon: Int?
    var iconPhrase: String?
    var wind: WeatherWind?
    
}

//
//  WeatherVisibility.swift
//  Sailing Support
//
//  Created by Danijel on 14/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherVisibility: NSObject {
    
    var value: Float?
    var unit: String?
    
    init(value: Float, unit: String) {
        self.value = value
        self.unit = unit
    }
    
}

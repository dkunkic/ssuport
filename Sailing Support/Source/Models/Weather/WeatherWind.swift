//
//  WeatherWind.swift
//  Sailing Support
//
//  Created by Danijel on 14/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherWind: NSObject {
    
    var speed: WeatherWindSpeed?
    var direction: WeatherWindDirection?
    
}

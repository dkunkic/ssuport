//
//  WeatherDayItem.swift
//  Sailing Support
//
//  Created by Danijel on 16/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherDayItem: NSObject {
    
    var epochDate: Int?
    var temperature: WeatherTemperature?
    var realFeelTemperature: WeatherTemperature?
    var weatherDay: WeatherDay?
    var weatherNight: WeatherDay?
    
}

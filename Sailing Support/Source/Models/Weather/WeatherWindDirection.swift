//
//  WeatherWindDirection.swift
//  Sailing Support
//
//  Created by Danijel on 14/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherWindDirection: NSObject {
    
    init(degrees: Float, localized: String) {
        self.degrees = degrees
        self.localized = localized
    }
    
    var degrees: Float?
    var localized: String?
    
}

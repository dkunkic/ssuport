//
//  LocationModel.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class LocationModel: NSObject, Mappable, NSCoding {
    
    var locId: String?
    var locationId: String?
    var categoryId: String?
    var galleryId: Int?
    var title: String?
    var intro: String?
    var youtube: String?
    var video: String?
    var phone: String?
    var address: String?
    var text: String?
    var lat: Float?
    var lon: Float?
    var imgSrc: String?
    var imgSrcSec: String?
    var icon: String?
    var showOnHomepage: String?
    var showOnMainMap: String?

    required init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        locId <- map["locId"]
        locationId <- map["locationId"]
        categoryId <- map["categoryId"]
        galleryId <- map["galleryId"]
        title <- map["title"]
        intro <- map["intro"]
        youtube <- map["youtube"]
        video <- map["video"]
        phone <- map["phone"]
        address <- map["address"]
        text <- map["text"]
        lat <- map["lat"]
        lon <- map["lon"]
        imgSrc <- map["imgSrc"]
        imgSrcSec <- map["imgSrcSec"]
        icon <- map["icon"]
        showOnHomepage <- map["showOnHomepage"]
        showOnMainMap <- map["showOnMainMap"]
    }
    
    override init() {
        locId = ""
        locationId = ""
        categoryId = ""
        galleryId = 0
        title = ""
        intro = ""
        youtube = ""
        video = ""
        phone = ""
        address = ""
        text = ""
        lat = 0.0
        lon = 0.0
        imgSrc = ""
        imgSrcSec = ""
        icon = ""
        showOnHomepage = ""
        showOnMainMap = ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.locId, forKey: "locId")
        aCoder.encode(self.locationId, forKey: "locationId")
        aCoder.encode(self.categoryId, forKey: "categoryId")
        aCoder.encode(self.galleryId, forKey: "galleryId")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.intro, forKey: "intro")
        aCoder.encode(self.youtube, forKey: "youtube")
        aCoder.encode(self.video, forKey: "video")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.text, forKey: "text")
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lon, forKey: "lon")
        aCoder.encode(self.imgSrc, forKey: "imgSrc")
        aCoder.encode(self.imgSrcSec, forKey: "imgSrcSec")
        aCoder.encode(self.icon, forKey: "icon")
        aCoder.encode(self.showOnHomepage, forKey: "showOnHomepage")
        aCoder.encode(self.showOnMainMap, forKey: "showOnMainMap")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        self.locId = (aDecoder.decodeObject(forKey: "locId") as? String) ?? ""
        self.locationId = (aDecoder.decodeObject(forKey: "locationId") as? String) ?? ""
        self.categoryId = (aDecoder.decodeObject(forKey: "categoryId") as? String) ?? ""
        self.galleryId = (aDecoder.decodeObject(forKey: "galleryId") as? Int) ?? 0
        self.title = (aDecoder.decodeObject(forKey: "title") as? String) ?? ""
        self.intro = (aDecoder.decodeObject(forKey: "intro") as? String) ?? ""
        self.youtube = (aDecoder.decodeObject(forKey: "youtube") as? String) ?? ""
        self.video = (aDecoder.decodeObject(forKey: "video") as? String) ?? ""
        self.phone = (aDecoder.decodeObject(forKey: "phone") as? String) ?? ""
        self.address = (aDecoder.decodeObject(forKey: "address") as? String) ?? ""
        self.text = (aDecoder.decodeObject(forKey: "text") as? String) ?? ""
        self.lat = (aDecoder.decodeObject(forKey: "lat") as? Float) ?? 0.0
        self.lon = (aDecoder.decodeObject(forKey: "lat") as? Float) ?? 0.0
        self.imgSrc = (aDecoder.decodeObject(forKey: "imgSrc") as? String) ?? ""
        self.imgSrcSec = (aDecoder.decodeObject(forKey: "imgSrcSec") as? String) ?? ""
        self.icon = (aDecoder.decodeObject(forKey: "icon") as? String) ?? ""
        self.showOnHomepage = (aDecoder.decodeObject(forKey: "showOnHomepage") as? String) ?? ""
        self.showOnMainMap = (aDecoder.decodeObject(forKey: "showOnMainMap") as? String) ?? ""
    }
    
}

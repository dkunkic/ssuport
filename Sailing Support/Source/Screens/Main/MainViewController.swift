//
//  ViewController.swift
//  Sailing Support
//
//  Created by Danijel on 09/11/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import UIKit

protocol MainProtocol: class {
    func openScreen(viewController: UIViewController)
    func screenDidLoad(view: UIView)
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: MainPresenter!
    var presentMenuAnimator: PresentMenuAnimator?
    
    // MArk: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = MainPresenter(view: self)
        setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    // MARK: - Navigation setup
    func setupNavigation() {
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor.init(red: 19/255, green: 26/255, blue: 34/255, alpha: 1)
        setupNavigationLeftItems()
    }
    
    func setupNavigationLeftItems() {
        
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.systemFont(ofSize: 26)
        titleLabel.text = "Sailing Support"
        
        let labelView = UIBarButtonItem(customView: titleLabel)
        
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        menuButton.setImage(UIImage(named: "menu"), for: .normal)
        menuButton.imageView?.contentMode = .scaleAspectFit
        menuButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
        menuButton.addTarget(self, action: #selector(openMenu(sender:)), for: .touchUpInside)
        
        let leftItem = UIBarButtonItem(customView: menuButton)
        self.navigationItem.leftBarButtonItems = [leftItem, labelView]
    }
    
    @objc func openMenu(sender: AnyObject) {
        performSegue(withIdentifier: "openMenu", sender: nil)
    }
    
    // MARK: - Side Menu setup
    let interactor = Interactor()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = interactor
            destinationViewController.mainProtocol = self
        }
    }
    
    @IBAction func edgePanGesture(sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .right)
        
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor) {
                self.performSegue(withIdentifier: "openMenu", sender: nil)
        }
    }
    
}

// MARK: - MainPresenterProtocol
extension MainViewController: MainPresenterProtocol {
    
    func downloadComplete() {
        self.activityIndicator.isHidden = true

        guard let vc = HomeScreen(mainDelegate: self).instantiateViewController() else { return }
        addChildViewController(vc)
        viewContainer.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
}

// MARK: - MainProtocol
extension MainViewController: MainProtocol {

    func openScreen(viewController: UIViewController) {
        for oldVc in viewContainer.subviews {
            oldVc.removeFromSuperview()
        }
        addChildViewController(viewController)
        viewContainer.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
    }
    
    func screenDidLoad(view: UIView) {
        if let presentMA = self.presentMenuAnimator {
            presentMA.changeSnapshot(view: view)
        }
    }
    
}

protocol PresentMenuProtocol {
    func updateSnapshotContainer()
}

// MARK: - UIViewControllerTransitioningDelegate
extension MainViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presentMenuAnimator = PresentMenuAnimator()
        return self.presentMenuAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

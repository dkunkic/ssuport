//
//  TestScreen.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class HomeScreen: BaseScreen {
    
    weak var mainDelegate: MainProtocol?
    
    init(mainDelegate: MainProtocol) {
        self.mainDelegate = mainDelegate
    }
    
    required init() {
        fatalError("error init")
    }
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? HomeViewController {
            let presenter = HomePresenter()
            vc.presenter = presenter
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "HomeViewController"
    }
    
}

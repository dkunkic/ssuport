//
//  TestViewController.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SDWebImage

class WeatherItemCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
}

class LocationModelCell: UICollectionViewCell {
    var locationModel: LocationModel?
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var containerView: UIView!
}

class HomeViewController: BaseViewController, HomeProtocol {
    
    @IBOutlet weak var locationTitle: UILabel!
    @IBOutlet weak var weatherTitle: UILabel!
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var temperatureSmallLabel: UILabel!
    @IBOutlet weak var temperatureFeelsLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var locationsCollectionView: UICollectionView!
    
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.setupWeather()
        presenter?.setupLocationModels()
        setupGoogleMapView()
    }
    
    func setupWeather(locationName: String) {
        locationTitle.text = locationName
    }
    
    func setupWeatherHours(items: [WeatherHourItem]) {
        let firstItem = items[0]
        weatherTitle.text = firstItem.iconPhrase
        if let icon = firstItem.weatherIcon {
            weatherImageView.image = UIImage(named: String(format: "w_%d", icon))
        }
        if let tempValue = firstItem.temperature?.value {
            temperatureLabel.text = String(format: "%.1f ℃", tempValue)
            temperatureSmallLabel.text = String(format: "%.1f ℃", tempValue)
        }
        if let feelsTempValue = firstItem.realFeelTemperature?.value {
            temperatureFeelsLabel.text = String(format: "Feels like %.1f ℃", feelsTempValue)
        }
        if let windSpeed = firstItem.wind?.speed?.value {
            if let windDirectionAngle = firstItem.wind?.direction?.degrees {
                if let windDirection = firstItem.wind?.direction?.localized {
                    windLabel.text = String(format: "%.1f km/h %.1f ° %@", windSpeed, windDirectionAngle, windDirection)
                }
            }
        }
        if let humidity = firstItem.relativeHumidity {
            humidityLabel.text = String(format: "Humidity: %d %%", humidity)
        }
        
        setupWeatherCollectionView()
    }
    
    func setupWeatherCollectionView() {
        weatherCollectionView.tag = 0
        weatherCollectionView.dataSource = self
        weatherCollectionView.delegate = self
    }
    
    func setupLocationModels() {
        locationsCollectionView.tag = 1
        locationsCollectionView.dataSource = self
        locationsCollectionView.delegate = self
    }
    
    func setupGoogleMapView() {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.googleMapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
        
        googleMapView.isUserInteractionEnabled = false
        googleMapView.mapType = .satellite
        googleMapView.isMyLocationEnabled = true

    }
    
}

extension HomeViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            guard let items = presenter?.weatherItems else {
                return 0
            }
            return items.count
        case 1:
            guard let items = presenter?.locationItems else {
                return 0
            }
            return items.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherItemCell", for: indexPath) as! WeatherItemCell // swiftlint:disable:this force_cast
            
            if let weatherItem = presenter?.weatherItems![indexPath.row] {
                if let tempValue = weatherItem.temperature?.value {
                    cell.temperatureLabel.text = String(format: "%.1f ℃", tempValue)
                }
                if let epochDateTime = weatherItem.epochDateTime {
                    let date = Date(timeIntervalSince1970: TimeInterval(epochDateTime))
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "HH:mm"
                    cell.hoursLabel.text = dateFormater.string(from: date)
                }
                if let icon = weatherItem.weatherIcon {
                    cell.weatherIcon.image = UIImage(named: String(format: "w_%d", icon))
                }
            }
            
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationModelCell", for: indexPath) as! LocationModelCell // swiftlint:disable:this force_cast
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOpacity = 1
            cell.containerView.layer.shadowOffset = CGSize.zero
            cell.containerView.layer.shadowRadius = 5
            if let items = presenter?.locationItems {
                let locationModel = items[indexPath.row]
                if let imageSrcSec = locationModel.imgSrcSec {
                    cell.imageVIew.sd_setImage(with: URL(string: String(format: "http://sailing.coreaplikacije.hr/%@", imageSrcSec.replacingOccurrences(of: "_90.", with: "_160."))), completed: nil)
                }
                if let title = locationModel.title {
                    cell.titleLabel.text = title
                }
                if let intro = locationModel.intro {
                    cell.textField.text = intro
                }
                
            }
            
            return cell
        default:
            return collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherItemCell", for: indexPath) as! WeatherItemCell // swiftlint:disable:this force_cast
        }
        
    }
    
}

extension HomeViewController: UICollectionViewDelegate {
    
}

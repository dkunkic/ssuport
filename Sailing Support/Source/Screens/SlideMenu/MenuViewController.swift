//
//  MenuViewController.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        guard let text = self.text else { return super.intrinsicContentSize }
        
        var contentSize = super.intrinsicContentSize
        var textWidth: CGFloat = frame.size.width
        var insetsHeight: CGFloat = 0.0
        
        if let insets = padding {
            textWidth -= insets.left + insets.right
            insetsHeight += insets.top + insets.bottom
        }
        
        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedStringKey.font: self.font], context: nil)
        
        contentSize.height = ceil(newSize.size.height) + insetsHeight
        
        return contentSize
    }
}

class MenuViewController: UIViewController {
    
    weak var mainProtocol: MainProtocol?
    var presenter: MenuPresenter!
    var selectedTableViewCell: Int?
    var mainStackView: UIStackView!
    
    var stackViewHeight: Int = 0
    
    @IBOutlet weak var menuScrollView: UIScrollView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MenuPresenter(view: self)
        setupMenuItems()
    }
    
    func setupMenuItems() {
        self.stackViewHeight = 220 + (presenter.getFixedMenuItems().count - 1) * 50 + (presenter.getMenuItems().count * 50)
        self.mainStackView = UIStackView(frame: CGRect(x: 0, y: 20, width: Int(self.menuScrollView.frame.size.width), height: self.stackViewHeight))
        self.mainStackView.axis = .vertical
        self.mainStackView.distribution = .equalCentering
        self.menuScrollView.addSubview(self.mainStackView)
        self.menuScrollView.contentSize = CGSize(width: self.mainStackView.frame.width, height: self.mainStackView.frame.height + 20)
        let fixedItems = presenter.getFixedMenuItems()
        for (index, fixedMenuCategory) in fixedItems.enumerated() {
            if index == 0 {
                createWeatherRow()
            } else {
                createRowForFixedItems(menuCategory: fixedMenuCategory, index: index)
            }
        }
        
        for (index, itemChild) in presenter.getMenuItems().enumerated() {
            createRowForFixedItems(menuCategory: itemChild, index: index)
        }
    }
    
    func createWeatherRow() {
        guard let weatherView = Bundle.main.loadNibNamed("WeatherView", owner: nil, options: nil)?.first as? WeatherView else { return }
        weatherView.heightAnchor.constraint(equalToConstant: 220).isActive = true
        
        WeatherService.sharedService.getHourItems().onSuccess { items in
            if let icon = items[0].weatherIcon {
                weatherView.iconImageView.image = UIImage(named: String(format: "w_%d", icon))
            }
            if let locationName = WeatherService.sharedService.localizedName {
                weatherView.locationNameLabel.text = locationName
            }
            if let currentWeather = items[0].iconPhrase {
                weatherView.currentWeather.text = currentWeather
            }
        }
        
        self.mainStackView.addArrangedSubview(weatherView)
    }
    
    func createRowForFixedItems(menuCategory: MenuCategory, index: Int) {
        guard let resizableView = Bundle.main.loadNibNamed("ResizableView", owner: nil, options: nil)?.first as? ResizableView else { return }
        resizableView.viewHeightAnchorConstraint = resizableView.heightAnchor.constraint(equalToConstant: 50)
        resizableView.viewHeightAnchorConstraint?.isActive = true
        resizableView.stackView.isHidden = true
        resizableView.categoryTitleLabel.text = menuCategory.categoryName
        resizableView.menuItem = menuCategory
        guard let icon = menuCategory.icon else { return }
        resizableView.imageView.image = UIImage(named: icon)
        resizableView.button.tag = menuCategory.categoryId!
        resizableView.button.addTarget(self, action: #selector(tapOnRow(sender:)), for: .touchUpInside)
        self.mainStackView.addArrangedSubview(resizableView)
    }
    
    func createChildItem(item: MenuCategory, iterationNum: Int) -> UIView {
        guard let resizableView = Bundle.main.loadNibNamed("ResizableView", owner: nil, options: nil)?.first as? ResizableView else { return UIView() }
        resizableView.viewHeightAnchorConstraint = resizableView.heightAnchor.constraint(equalToConstant: 50)
        resizableView.viewHeightAnchorConstraint?.isActive = true
        resizableView.backgroundColor = UIColor.gray
        
        resizableView.button.addTarget(self, action: #selector(tapOnRow(sender:)), for: .touchUpInside)
        resizableView.button.tag = item.categoryId!
        
        resizableView.categoryTitleLabel.padding = UIEdgeInsets(top: 0, left: CGFloat(20 + (20 * iterationNum)), bottom: 0, right: 0)
        resizableView.iterationNum = iterationNum + 1
        resizableView.categoryTitleLabel.text = item.categoryName
        
        resizableView.menuItem = item
        
        return resizableView
    }
    
    @objc func tapOnRow(sender: UIButton) {
        var child = getChildFromItems(items: presenter.getFixedMenuItems(), id: sender.tag)
        if child == nil {
            child = getChildFromItems(items: presenter.getMenuItems(), id: sender.tag)
        } else {
            //todo home, weather forecast etc...
            return
        }
        
        guard let safeChild = child else { return }
        guard let safeChildren = safeChild.children else {
            //todo
            return
        }
        if safeChildren.count == 0 { return }
        guard let resizableView = sender.superview as? ResizableView else {
            //todo
            return
        }
        
        if let menuCategory = resizableView.menuItem {
            if menuCategory.locationId != nil && menuCategory.locationId != -1 {
                
            } else {
                if let categoryId = menuCategory.categoryId {
                    guard let vc = LocationsMapScreen(mainDelegate: self.mainProtocol! ,categoryId: categoryId).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                }
            }
        }
        
        didTapOnResizableViewWithChild(children: safeChildren, childrenCount: safeChildren.count, resizableView: resizableView)
    }
    
    func getChildFromItems(items: [MenuCategory]?, id: Int) -> MenuCategory? {
        guard let children = items else { return nil }
        for child in children {
            if child.categoryId == id {
                return child
            }
            let child = getChildFromItems(items: child.children, id: id)
            if child != nil { return child }
        }
        return nil
    }
    
    func didTapOnResizableViewWithChild(children: [MenuCategory], childrenCount: Int, resizableView: ResizableView) {
        if resizableView.isExtended {
            for subview in resizableView.stackView.subviews {
                subview.removeFromSuperview()
            }
        } else {
            for smallChild in children {
                resizableView.stackView.addArrangedSubview(createChildItem(item: smallChild, iterationNum: resizableView.iterationNum))
            }
        }
        var newMainStackHeight: CGFloat = 0
        var newRowItemHeight: CGFloat = 0
        if resizableView.isExtended {
            newRowItemHeight = 50
            newMainStackHeight = self.mainStackView.frame.height - resizableView.frame.height + newRowItemHeight
        } else {
            newRowItemHeight = CGFloat(Int(resizableView.frame.size.height) + (childrenCount * 50))
            newMainStackHeight = self.mainStackView.frame.height + newRowItemHeight - 50
        }
        resizableView.viewHeightAnchorConstraint?.constant = newRowItemHeight
        resizableView.previousHeight = newRowItemHeight
        resizableView.isExtended = !resizableView.isExtended
        changeReizableViewParentsHeight(resizableView: resizableView, newRowItemHeight: newRowItemHeight)
        resizableView.stackView.isHidden = !resizableView.isExtended
        self.mainStackView.frame = CGRect(x: self.mainStackView.frame.origin.x,
                                          y: self.mainStackView.frame.origin.y,
                                          width: self.mainStackView.frame.width,
                                          height: newMainStackHeight)
        self.menuScrollView.contentSize = CGSize(width: self.mainStackView.frame.width, height: self.mainStackView.frame.height + 20)
    }
    
    func changeReizableViewParentsHeight(resizableView: ResizableView, newRowItemHeight: CGFloat) {
        guard let parentView = resizableView.superview?.superview as? ResizableView else {
            return
        }
        parentView.viewHeightAnchorConstraint?.constant = resizableView.isExtended ? parentView.previousHeight + (resizableView.viewHeightAnchorConstraint?.constant)! - 50 : parentView.previousHeight
        if (parentView.superview?.superview as? ResizableView) != nil {
            changeReizableViewParentsHeight(resizableView: parentView, newRowItemHeight: newRowItemHeight)
        }
        
    }
    
    // MARK: - Interaction
    var interactor: Interactor?
    
    @IBAction func handleGesture(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        let progress = MenuHelper.calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .left
        )
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor) {
                self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeMenu(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - MenuPresenterProtocol
extension MenuViewController: MenuPresenterProtocol {
    
    func openScreen(viewController: UIViewController) {
        mainProtocol?.openScreen(viewController: viewController)
    }
    
    func itemsReady() {
        //menuTableView.reloadData()
    }
    
}

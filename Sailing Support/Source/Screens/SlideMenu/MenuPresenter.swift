//
//  MenuPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

protocol MenuPresenterProtocol: class {
    func openScreen(viewController: UIViewController)
    func itemsReady()
}

class MenuPresenter {
    
    weak var view: MenuPresenterProtocol?
    
    var menuItems: [MenuCategory]?
    
    init(view: MenuPresenterProtocol) {
        self.view = view
        self.menuItems = AppService.sharedService.app.menuItems
        self.view?.itemsReady()
    }
    
    func getMenuItemsCount() -> Int {
        guard let itemsCount = menuItems?.count else {
            return 0
        }
        return itemsCount
    }
    
    func getMenuItems() -> [MenuCategory] {
        guard let items = self.menuItems else {
            return []
        }
        return items
    }
    
    func getFixedMenuItems() -> [MenuCategory] {
        var fixedMenuItems = [MenuCategory]()
        
        fixedMenuItems.append(MenuCategory())
        fixedMenuItems.append(MenuCategory(categoryName: "Home", categoryId: -1, icon: "ic_home"))
        fixedMenuItems.append(MenuCategory(categoryName: "Weather Forecast", categoryId: -2, icon: "ic_weather_forecast"))
        fixedMenuItems.append(MenuCategory(categoryName: "Navigation & Sailing", categoryId: -3, icon: "ic_navigation"))
        
        let skyMap = MenuCategory(categoryName: "SkyMap", categoryId: -6, icon: "")
        let issTracker = MenuCategory(categoryName: "ISS Tracker", categoryId: -7, icon: "")
        fixedMenuItems.append(MenuCategory(categoryName: "Night Sky", categoryId: -4, icon: "ic_night_sky", children: [skyMap, issTracker]))
        
        let routes = MenuCategory(categoryName: "Routes", categoryId: -8, icon: "")
        fixedMenuItems.append(MenuCategory(categoryName: "Trip", categoryId: -5, icon: "ic_trip", children: [routes]))
        
        return fixedMenuItems
    }
    
    func getAllChildrenCount() -> Int {
        var size = 0
        for child in self.getFixedMenuItems() {
            size = getSize(child: child, size: size)
        }
        for child in self.getMenuItems() {
            size = getSize(child: child, size: size)
        }
        return size
    }
    
    func getSize(child: MenuCategory, size: Int) -> Int {
        var newSize = size
        newSize += 1
        guard let children = child.children else {
            return newSize
        }
        
        for smallChild in children {
            newSize = getSize(child: smallChild, size: newSize)
        }
        return newSize
    }
    
    func getItemTitle(index: Int) -> String {
        guard let items = menuItems else {
            return ""
        }
        guard let categoryName = items[index].categoryName else {
            return ""
        }
        return categoryName
    }
    
    func getItemImageName(index: Int) -> String {
        guard let items = menuItems else {
            return ""
        }
        guard let icon = items[index].icon else {
            return ""
        }
        return icon
    }
    
    func isItemChildrenEmpty(index: Int) -> Bool {
        guard let items = menuItems else {
            return true
        }
        guard let children = items[index].children else {
            return true
        }
        return children.isEmpty
    }
    
    func getItemChildren(index: Int) -> [MenuCategory] {
        guard let items = menuItems else {
            return []
        }
        guard let children = items[index].children else {
            return []
        }
        return children
    }
    
    func openTestScreen() {
        //view?.openScreen(viewController: TestScreen().instantiateViewController())
    }
}

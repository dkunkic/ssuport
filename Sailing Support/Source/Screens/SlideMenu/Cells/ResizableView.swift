//
//  ResizableTableViewCell.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class ResizableView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var button: UIButton!
    
    var viewHeightAnchorConstraint: NSLayoutConstraint?
    var previousHeight: CGFloat = 0
    var isExtended: Bool = false
    var iterationNum: Int = 0
    
    var menuItem: MenuCategory?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }

    
}

//
//  BaseViewController.swift
//  Sailing Support
//
//  Created by Danijel on 09/11/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    var mainVCProtocol: MainProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

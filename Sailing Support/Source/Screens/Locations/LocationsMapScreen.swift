//
//  LocationsMapScreen.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class LocationsMapScreen: BaseScreen {
    
    weak var mainDelegate: MainProtocol?
    var categoryId: Int!
    
    init(mainDelegate: MainProtocol, categoryId: Int) {
        self.mainDelegate = mainDelegate
        self.categoryId = categoryId
    }
    
    required init() {
        fatalError("error init")
    }
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? LocationsMapViewController {
            let presenter = LocationsMapPresenter(categoryId: self.categoryId)
            vc.presenter = presenter
            vc.mainVCProtocol = self.mainDelegate
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "LocationsMapViewController"
    }
    
}


//
//  LocationsMap.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class LocationsMapViewController: BaseViewController, LocationsMapProtocol {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
    var presenter: LocationsMapPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.getLocationModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
}

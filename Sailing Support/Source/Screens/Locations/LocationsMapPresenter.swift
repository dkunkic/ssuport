//
//  LocationsMapPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

protocol LocationsMapProtocol: class {
    
}

class LocationsMapPresenter {
    
    weak var view: LocationsMapProtocol?
    var categoryId: Int!
    
    init(categoryId: Int) {
        self.categoryId = categoryId
    }
    
    func getLocationModel() {
        
    }
    
}

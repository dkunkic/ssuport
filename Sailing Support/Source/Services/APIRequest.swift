//
//  APIRequest.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import Alamofire

class APIRequest {
    
    var endpoint: String = ""
    var parameters: [String: AnyObject]?
    var method: HTTPMethod = .get
    var upload: Bool = false
    
    init() {
        
    }
    
    convenience init(endpoint: String, method: HTTPMethod = .get, parameters: [String: AnyObject]? = nil, upload: Bool = false) {
        self.init()
        self.endpoint = endpoint
        self.method = method
        self.parameters = parameters
        self.upload = upload
    }
    
}

//
//  LocationService.swift
//  Sailing Support
//
//  Created by Danijel on 16/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

import CoreLocation

protocol LocationServiceProtocol: class {
    func locationFetched()
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var locationStatusEnum: CLAuthorizationStatus?
    
    var location: CLLocation?
    
    weak var locationServiceProtocol: LocationServiceProtocol?
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationStatusEnum = status
        switch status {
        case .notDetermined:
            break
            
        case .authorizedAlways:
            self.locationManager.startUpdatingLocation()
        case .denied:
            break
            
        default:
            break
            
        }
    }
    
    func startUpdatingLocation(locProtocol: LocationServiceProtocol) {
        self.locationServiceProtocol = locProtocol
        self.locationManager.distanceFilter = 100
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last as CLLocation?
        self.location = location
        if let locProtocol = self.locationServiceProtocol {
            locProtocol.locationFetched()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) { }
    
}

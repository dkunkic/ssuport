//
//  LocationsService.swift
//  Sailing Support
//
//  Created by Danijel on 17/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import BrightFutures
import SwiftyJSON
import Alamofire
import AlamofireObjectMapper

class LocationsService {
    
    static let GET_ALL_LOCATIONS_URL = Constants.BASE_URL  + "/MobileMenuCategories/getAllLocations"
    
    static let sharedService = LocationsService()
    
    var locationsItems: [LocationModel]?
    
    func downloadLocationsData() -> Future<Bool, NSError> {
        let promise = Promise<Bool, NSError>()
        
        Alamofire.request(LocationsService.GET_ALL_LOCATIONS_URL).responseObject { (response: DataResponse<LocationRootModel>) in
            
            let locationRootModel = response.result.value
            if let items = locationRootModel?.root {
                self.locationsItems = items
                AppService.sharedService.app.locationModels = items
                AppService.sharedService.saveApp()
                promise.success(true)
            } else {
                promise.success(false)
            }
            
        }
        
        return promise.future
    }
    
}

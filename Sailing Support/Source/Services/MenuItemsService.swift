//
//  MenuItemsService.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import BrightFutures
import SwiftyJSON

class MenuItemsService {
    
    static let MENUS_ITEMS_URL = Constants.BASE_URL + "MobileMenuCategories/getMenuByParentId"
    
    static let sharedService = MenuItemsService()
    
    func downloadMenuItems() -> Future<[MenuCategory], NSError> {
        let promise = Promise<[MenuCategory], NSError>()
        
        let request = APIRequest(endpoint: MenuItemsService.MENUS_ITEMS_URL, method: .get, parameters: nil, upload: false)
        APIService.sharedService.request(request)
            .onSuccess(callback: { result in
                let menuItems = self.parseResult(result: JSON(result))
                promise.success(menuItems)
            })
            .onFailure(callback: { error in
                promise.failure(error)
            }
        )
        
        return promise.future
    }
    
    func parseResult(result: JSON) -> [MenuCategory] {
        var menus: [MenuCategory] = []
        for (_, item) in result["root"] {
            menus.append(parseMenuItem(item: item))
        }
        return menus
    }
    
    func parseMenuItem(item: JSON) -> MenuCategory {
        let menuCategory = MenuCategory()
        
        menuCategory.categoryId = parseInt(item: item, key: "categoryId")
        menuCategory.parentId = parseInt(item: item, key: "parentId")
        menuCategory.categoryName = parseString(item: item, key: "categoryName")
        menuCategory.categoryDesc = parseString(item: item, key: "categoryDesc")
        menuCategory.customPage = parseString(item: item, key: "customPage")
        menuCategory.imgSrc = parseString(item: item, key: "imgSrc")
        menuCategory.wikiCategoryId = parseInt(item: item, key: "wikiCategoryId")
        menuCategory.templateId = parseInt(item: item, key: "templateId")
        menuCategory.locationId = parseInt(item: item, key: "locationId")
        menuCategory.iconId = parseInt(item: item, key: "iconId")
        menuCategory.display = parseInt(item: item, key: "display")
        menuCategory.newWindow = parseInt(item: item, key: "newWindow")
        menuCategory.orderNum = parseInt(item: item, key: "orderNum")
        menuCategory.wikiId = parseInt(item: item, key: "wikiId")
        menuCategory.name = parseString(item: item, key: "name")
        menuCategory.icon = parseString(item: item, key: "icon")
        
        if item["children"].exists() && !item["children"].isEmpty {
            var children: [MenuCategory] = []
            for (_, childItem) in item["children"] {
                children.append(parseMenuItem(item: childItem))
            }
            menuCategory.children = children
        }
        
        return menuCategory
    }
    
    func parseString(item: JSON, key: String) -> String {
        guard let rawValue = item[key].rawString() else {
            return ""
        }
        return rawValue
    }
    
    func parseInt(item: JSON, key: String) -> Int {
        guard let rawValue = item[key].rawString() else {
            return -1
        }
        guard let intValue = Int(rawValue) else {
            return -1
        }
        return intValue
    }
 
}

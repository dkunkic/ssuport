//
//  WeatherService.swift
//  Sailing Support
//
//  Created by Danijel on 16/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import BrightFutures
import SwiftyJSON
import Alamofire

class WeatherService {
    
    static let WEATHER_HOURLY_URL = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/%@?apikey=o1w7TrWJqSV37G5be2z2sXolSghAbeV5&language=en-us&details=true&metric=true"
    static let WEATHER_DAILY_URL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/%@?apikey=o1w7TrWJqSV37G5be2z2sXolSghAbeV5&language=en-us&details=true&metric=true"
    
    static let sharedService = WeatherService()
    
    var hourItems: [WeatherHourItem]?
    var dayItems: [WeatherDayItem]?
    var localizedName: String?
    var locationKey: String?
    
    func downloadWeatherData() -> Future<Bool, NSError> {
        let promise = Promise<Bool, NSError>()
        
        self.downloadLocationKey()
            .onSuccess { key in
                var promiseSequence = [Future<String, NSError>]()
                promiseSequence.append(self.downloadWeatherDailyData(key: key))
                promiseSequence.append(self.downloadWeatherHourlyData(key: key))
                promiseSequence.sequence().onSuccess(callback: { string in
                    promise.success(true)
                })
            }.onFailure { error in
                promise.failure(error)
            }
        
        return promise.future
    }
    
    func getHourItems() -> Future<[WeatherHourItem], NSError> {
        let promise = Promise<[WeatherHourItem], NSError>()
        
        if let items = self.hourItems {
            promise.success(items)
        } else {
            promise.failure(NSError(domain: "failed", code: 1, userInfo: nil))
        }
        
        return promise.future
    }
    
    func getDayItems() -> Future<[WeatherDayItem], NSError> {
        let promise = Promise<[WeatherDayItem], NSError>()
        if let items = self.dayItems {
            promise.success(items)
        } else {
            promise.failure(NSError(domain: "failed", code: 1, userInfo: nil))
        }
        return promise.future
    }
    
    func downloadWeatherDailyData(key: String) -> Future<String, NSError> {
        let promise = Promise<String, NSError>()
        
        let url = String(format: WeatherService.WEATHER_DAILY_URL, key)
        
        let request = APIRequest(endpoint: url, method: .get, parameters: nil, upload: false)
        
        APIService.sharedService.request(request)
            .onSuccess(callback: { result in
                self.dayItems = self.parseResult(result: JSON(result))
                promise.success("menuItems")
            })
            .onFailure(callback: { error in
                promise.failure(error)
            }
        )
        
        return promise.future
    }
    
    func downloadWeatherHourlyData(key: String) -> Future<String, NSError> {
        let promise = Promise<String, NSError>()
        
        let url = String(format: WeatherService.WEATHER_HOURLY_URL, key)
        
        Alamofire.request(url, method: .get).responseJSON(completionHandler: { response in
            _ = response.map { json in
                let weatherJson = JSON(json)
                
                var items = [WeatherHourItem]()
                for (_, subJson): (String, JSON) in weatherJson {
                    let item = WeatherHourItem()
                    
                    item.epochDateTime = subJson["EpochDateTime"].int
                    item.weatherIcon = subJson["WeatherIcon"].int
                    item.iconPhrase = subJson["IconPhrase"].string
                    
                    let temperature = WeatherTemperature()
                    temperature.unit = subJson["Temperature"]["Unit"].string
                    temperature.unityType = subJson["Temperature"]["UnitType"].int
                    temperature.value = subJson["Temperature"]["Value"].float
                    item.temperature = temperature
                    
                    let realFeelTemperature = WeatherTemperature()
                    realFeelTemperature.unit = subJson["RealFeelTemperature"]["Unit"].string
                    realFeelTemperature.unityType = subJson["RealFeelTemperature"]["UnitType"].int
                    realFeelTemperature.value = subJson["RealFeelTemperature"]["Value"].float
                    item.realFeelTemperature = realFeelTemperature
                    
                    let wind = WeatherWind()
                    wind.speed = WeatherWindSpeed(value: subJson["Wind"]["Speed"]["Value"].float!, unit: subJson["Wind"]["Speed"]["Unit"].string!, unitType: subJson["Wind"]["Speed"]["UnitType"].int!)
                    wind.direction = WeatherWindDirection(degrees: subJson["Wind"]["Direction"]["Degrees"].float!, localized: subJson["Wind"]["Direction"]["Localized"].string!)
                    item.wind = wind
                    
                    item.relativeHumidity = subJson["RelativeHumidity"].int
                    
                    let visibility = WeatherVisibility(value: subJson["Visibility"]["Value"].float!, unit: subJson["Visibility"]["Unit"].string!)
                    item.visibility = visibility
                    
                    item.uvIndex = subJson["UVIndex"].int
                    
                    items.append(item)
                }
                self.hourItems = items
                promise.success("items")
            }
        })
        
        return promise.future
    }
    
    func downloadLocationKey() -> Future<String, NSError> {
        let promise = Promise<String, NSError>()
        
        let url = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=o1w7TrWJqSV37G5be2z2sXolSghAbeV5&language=en-us&details=true&toplevel=false"
//        let latLong = String(format: "%@,%@", location.coordinate.latitude.description)
        if let location = AppService.sharedService.locationService.location {
            let latString = location.coordinate.latitude.description.replacingOccurrences(of: ",", with: ".")
            let longString = location.coordinate.longitude.description.replacingOccurrences(of: ",", with: ".")
            let latLong = String(format: "%@,%@", latString, longString)
            Alamofire.request(url, method: .get, parameters: ["q": latLong]).responseJSON(completionHandler: { response in
                _ = response.map { json in
                    let weatherJson = JSON(json)
                    if let locationName = weatherJson["LocalizedName"].string {
                        self.localizedName = locationName
                        if let key = weatherJson["Key"].string {
                            self.locationKey = key
                            promise.success(key)
                        }
                    }
                }
            })
        } else {
            promise.failure(NSError( domain: "error with location", code: 0, userInfo: nil))
        }
        
        return promise.future
    }
    
    func parseResult(result: JSON) -> [WeatherDayItem] {
        var items = [WeatherDayItem]()
        
        for (_, subJson): (String, JSON) in result["DailyForecasts"] {
            let item = WeatherDayItem()
            
            item.epochDate = subJson["EpochDate"].int
            
            let temperature = WeatherTemperature()
            temperature.unit = subJson["Temperature"]["Unit"].string
            temperature.unityType = subJson["Temperature"]["UnitType"].int
            temperature.value = subJson["Temperature"]["Value"].float
            item.temperature = temperature
            
            let realFeelTemperature = WeatherTemperature()
            realFeelTemperature.unit = subJson["RealFeelTemperature"]["Unit"].string
            realFeelTemperature.unityType = subJson["RealFeelTemperature"]["UnitType"].int
            realFeelTemperature.value = subJson["RealFeelTemperature"]["Value"].float
            item.realFeelTemperature = realFeelTemperature
            
            let weatherDay = WeatherDay()
            weatherDay.icon = subJson["Day"]["Icon"].int
            weatherDay.iconPhrase = subJson["Day"]["IconPhrase"].string
            let dayWind = WeatherWind()
            dayWind.speed = WeatherWindSpeed(value: subJson["Day"]["Wind"]["Speed"]["Value"].float!, unit: subJson["Day"]["Wind"]["Speed"]["Unit"].string!, unitType: subJson["Day"]["Wind"]["Speed"]["UnitType"].int!)
            dayWind.direction = WeatherWindDirection(degrees: subJson["Day"]["Wind"]["Direction"]["Degrees"].float!, localized: subJson["Day"]["Wind"]["Direction"]["Localized"].string!)
            weatherDay.wind = dayWind
            item.weatherDay = weatherDay
            
            let weatherNight = WeatherDay()
            weatherNight.icon = subJson["Night"]["Icon"].int
            weatherNight.iconPhrase = subJson["Night"]["IconPhrase"].string
            let nightWind = WeatherWind()
            nightWind.speed = WeatherWindSpeed(value: subJson["Night"]["Wind"]["Speed"]["Value"].float!, unit: subJson["Night"]["Wind"]["Speed"]["Unit"].string!, unitType: subJson["Night"]["Wind"]["Speed"]["UnitType"].int!)
            nightWind.direction = WeatherWindDirection(degrees: subJson["Night"]["Wind"]["Direction"]["Degrees"].float!, localized: subJson["Night"]["Wind"]["Direction"]["Localized"].string!)
            weatherNight.wind = nightWind
            item.weatherNight = weatherNight
            
            items.append(item)
        }
        
        return items
    }
    
}
